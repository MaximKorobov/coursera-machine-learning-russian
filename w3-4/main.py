import operator
import pandas
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, roc_auc_score, \
    precision_recall_curve


def save_answer(filename, text):
    print("Save to {0}: {1}".format(filename, text))
    text_file = open(filename, "w")
    text_file.write(str(text))
    text_file.close()


def main():
    classification = pandas.read_csv("classification.csv")

    tp = classification[(classification.pred == 1) & (classification.true == 1)].count().true
    fp = classification[(classification.pred == 1) & (classification.true == 0)].count().true
    fn = classification[(classification.pred == 0) & (classification.true == 1)].count().true
    tn = classification[(classification.pred == 0) & (classification.true == 0)].count().true
    print("tp, fp, fn, tn")
    save_answer("q1.txt", "{0} {1} {2} {3}".format(tp, fp, fn, tn))

    acc = accuracy_score(classification.true, classification.pred)
    prec = precision_score(classification.true, classification.pred)
    recall = recall_score(classification.true, classification.pred)
    f1 = f1_score(classification.true, classification.pred)
    print("acc, prec, recall, f1")
    save_answer("q2.txt", "{0} {1} {2} {3}".format(acc, prec, recall, f1))

    scores_data = pandas.read_csv("scores.csv")
    scores = {}
    for clf in scores_data.columns[1:]:
        scores[clf] = roc_auc_score(scores_data.true, scores_data[clf])
    scores_sorted = sorted(scores.items(), key=operator.itemgetter(1))
    name, value = scores_sorted[-1]
    save_answer("q3.txt", "{0}".format(name))

    pr_scores = {}
    for clf in scores_data.columns[1:]:
        pr_curve = precision_recall_curve(scores_data.true, scores_data[clf])
        pr_curve_df = pandas.DataFrame({'precision': pr_curve[0], 'recall': pr_curve[1]})
    pr_scores[clf] = pr_curve_df[pr_curve_df['recall'] >= 0.7]['precision'].max()
    pr_scores_sorted = sorted(pr_scores.items(), key=operator.itemgetter(1))
    name, value = pr_scores_sorted[-1]
    save_answer("q4.txt", "{0}".format(name))

main()
