import numpy as np
import pandas
from skimage.io import imread, imsave
import skimage
from sklearn.cluster import KMeans

image_raw = imread('parrots.jpg')
image = skimage.img_as_float(image_raw)

width, height, depth = image.shape
image_2d = np.reshape(image, (width*height, depth)) # 3d into 2d
pixels = pandas.DataFrame(image_2d, columns=["Red", "Green", "Blue"])


def cluster(pixels, n_clusters=8):
    print('Clustering: ' + str(n_clusters))

    pixels = pixels.copy()
    model = KMeans(n_clusters=n_clusters, init='k-means++', random_state=241)
    pixels['cluster'] = model.fit_predict(pixels)

    means = pixels.groupby('cluster').mean().values
    mean_pixels = [means[c] for c in pixels['cluster'].values]
    mean_image = np.reshape(mean_pixels, (width, height, depth))
    imsave('mean_parrots_' + str(n_clusters) + '.jpg', mean_image)

    medians = pixels.groupby('cluster').median().values
    median_pixels = [medians[c] for c in pixels['cluster'].values]
    median_image = np.reshape(median_pixels, (width, height, depth))
    imsave('median_parrots_' + str(n_clusters) + '.jpg', median_image)

    return mean_image, median_image

def psnr(image1, image2):
    mse = np.mean((image1 - image2) ** 2)
    return 10 * np.math.log10(float(1) / mse)


for n in range(1, 21):
    mean_image, median_image = cluster(pixels, n)
    psnr_mean, psnr_median = psnr(image, mean_image), psnr(image, median_image)
    print(psnr_mean, psnr_median)

    if psnr_mean > 20 or psnr_median > 20:
        print(n)
        break