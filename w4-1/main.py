import pandas
from py._builtin import text
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from scipy.sparse import hstack
from sklearn.linear_model import Ridge


def save_answer(filename, text):
    print("Save to {0}: {1}".format(filename, text))
    text_file = open(filename, "w")
    text_file.write(str(text))
    text_file.close()


def prepare_data(data):
    data['FullDescription'] = data['FullDescription'].apply(text.lower)
    data['FullDescription'] = data['FullDescription'].replace('[^a-zA-Z0-9]', ' ', regex=True)
    return data


def main():
    train = pandas.read_csv("salary-train.csv")

    # prepare data
    train = prepare_data(train)

    vector = TfidfVectorizer(min_df=5)
    x_train = vector.fit_transform(train['FullDescription'])

    train['LocationNormalized'].fillna("nan", inplace=True)
    train['ContractTime'].fillna("nan", inplace=True)

    dict_vector = DictVectorizer()
    x_train_dict = dict_vector.fit_transform(train[['LocationNormalized', 'ContractTime']].to_dict('records'))

    x_train_hstack = hstack([x_train, x_train_dict])

    y_train = train['SalaryNormalized']
    clf = Ridge(alpha=1, random_state=241)
    clf.fit(x_train_hstack, y_train)

    # test data
    test = pandas.read_csv('salary-test-mini.csv')
    test = prepare_data(test)
    x_test = vector.transform(test['FullDescription'])
    x_test_dict = dict_vector.transform(test[['LocationNormalized', 'ContractTime']].to_dict('records'))
    x_test_hstack = hstack([x_test, x_test_dict])

    y_test = clf.predict(x_test_hstack)

    save_answer("q1.txt", "{:0.2f} {:0.2f}".format(y_test[0], y_test[1]))

main()

