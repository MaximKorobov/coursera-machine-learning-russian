import os

import numpy
import pandas


def save_answer(filename, text):
    print("Save to {0}: {1}".format(filename, text))
    text_file = open(filename, "w")
    text_file.write(text)
    text_file.close()


def calculate_q1(data):
    male = 0
    female = 0
    for index, row in data.iterrows():
        if row["Sex"] == "male":
            male += 1
        elif row["Sex"] == "female":
            female += 1
    return "{0} {1}".format(male, female)


def calculate_q2(data):
    survived = 0
    total = 0
    for index, row in data.iterrows():
        total += 1
        if row["Survived"] == 1:
            survived += 1

    percents = round((survived / total) * 100, 2)
    return "{0}".format(percents)


def calculate_q3(data):
    first_class = 0
    total = 0
    for index, row in data.iterrows():
        total += 1
        if row["Pclass"] == 1:
            first_class += 1

    percents = round((first_class / total) * 100, 2)
    return "{0}".format(percents)


def calculate_q4(data):
    mean = numpy.mean(data.Age)
    median = data.Age.median()
    return "{0:.2f} {1:.2f}".format(mean, median)


def calculate_q5(data):
    corr = data['SibSp'].corr(data['Parch'], method='pearson')
    return "{0:.2f}".format(corr)


def extract_inner(name):
    found_something = 0
    lower_index = len(name)
    keys = " ()"

    for key in keys:
        index = name.find(key)
        if index != -1 and index < lower_index:
            lower_index = index
            found_something = 1

    if found_something == 1:
        return name[:lower_index]
    else:
        return name


def extract_name(name):
    word = ""

    # Если есть скобки, то первый элемент в скобках
    # Иначе первый элемент после miss. или mrs
    examples = ["(", "Miss. "]

    for example in examples:
        start = name.find(example)
        if start != -1:
            word = name[start+len(example):len(name)]
            word = extract_inner(word)
            break

    return word


def most_common(lst):
    return max(((item, lst.count(item)) for item in set(lst)), key=lambda a: a[1])[0]


def calculate_q6(data):
    names = []

    for index, row in data.iterrows():
        if row["Sex"] == "female":
            full_name = row["Name"]
            short_name = extract_name(full_name)
            names.append(short_name)

    result = most_common(names)

    return "{0}".format(result)


def main():
    data = pandas.read_csv("titanic.csv", index_col="PassengerId")

    methods = [calculate_q1, calculate_q2, calculate_q3, calculate_q4, calculate_q5, calculate_q6]

    for index, method in enumerate(methods):
        a = "q{0}.txt".format(index + 1)
        save_data = method(data)
        save_answer(a, save_data)


main()
