import pandas
import numpy
from sklearn.decomposition import PCA


def save_answer(filename, text):
    print("Save to {0}: {1}".format(filename, text))
    text_file = open(filename, "w")
    text_file.write(str(text))
    text_file.close()


def main():
    data = pandas.read_csv("close_prices.csv")
    x = data.loc[:, 'AXP':]

    pca = PCA(n_components=10)
    pca.fit(x)
    print(pca.explained_variance_ratio_)

    count = 0
    total = 0
    for el in pca.explained_variance_ratio_:
        total += el
        count += 1
        if total > 0.9:
            break
    save_answer("q1.txt", count)

    comp0 = pandas.DataFrame(pca.transform(x))[0]

    dj_prices = pandas.read_csv("djia_index.csv")
    dji = dj_prices['^DJI']

    coeff = numpy.corrcoef(comp0, dji)
    print(coeff)
    save_answer("q2.txt", coeff[1, 0])

    comp0_w = pandas.Series(pca.components_[0])
    comp0_w_top = comp0_w.sort_values(ascending=False).head(1).index[0]
    print(comp0_w)
    company = x.columns[comp0_w_top]
    save_answer("q3.txt", company)

main()
