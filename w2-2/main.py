import pandas
from numpy import linspace
from sklearn import datasets, preprocessing, neighbors, cross_validation


def save_answer(filename, text):
    print("Save to {0}: {1}".format(filename, text))
    text_file = open(filename, "w")
    text_file.write(str(text))
    text_file.close()


def get_accuracy(cv, x, y):
    result = []
    p_range = linspace(1, 10, 200)
    for p in p_range:
        model = neighbors.KNeighborsRegressor(p=p, n_neighbors=5, weights='distance')
        result.append(cross_validation.cross_val_score(model, x, y, 'mean_squared_error', cv))

    return pandas.DataFrame(result, p_range).max(axis=1).sort_values(ascending=False)


def main():
    data_set = datasets.load_boston()

    x = data_set.data
    y = data_set.target

    x = preprocessing.scale(x)

    cv = cross_validation.KFold(len(y), n_folds=5, shuffle=True, random_state=42)

    accuracies = get_accuracy(cv, x, y)
    save_answer("q1.txt", accuracies.index[0])


main()
