import numpy
import pandas
from sklearn.ensemble import RandomForestRegressor
from sklearn.cross_validation import KFold, cross_val_score


def main():
    df = pandas.read_csv("abalone.csv")
    df['Sex'].replace({'F': -1, 'I': 0, 'M': 1}, inplace=True)

    x = df.loc[:, 'Sex':'ShellWeight']
    y = df['Rings']

    kf = KFold(y.size, n_folds=5, shuffle=True, random_state=1)

    scores = [0.0]
    for trees in range(1, 51):
        print("trees is {0}".format(trees))
        model = RandomForestRegressor(n_estimators=trees, random_state=1)
        score = numpy.mean(cross_val_score(model, x, y, cv=kf, scoring='r2'))
        scores.append(score)

    for n, score in enumerate(scores):
        if score > 0.52:
            print("n is {0}".format(n))
            break

main()
