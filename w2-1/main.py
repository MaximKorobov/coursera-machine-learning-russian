import pandas
from sklearn import neighbors, cross_validation, preprocessing


def save_answer(filename, text):
    print("Save to {0}: {1}".format(filename, text))
    text_file = open(filename, "w")
    text_file.write(str(text))
    text_file.close()


def get_accuracy(cv, x, y):
    result = []
    k_range = range(1, 51)
    for k in k_range:
        classifier = neighbors.KNeighborsClassifier(k)
        result.append(cross_validation.cross_val_score(classifier, x, y, 'accuracy', cv))

    return pandas.DataFrame(result, k_range).mean(axis=1).sort_values(ascending=False)


def main():
    data = pandas.read_csv("wine.data", header=None)
    y = data[0]
    x = data.loc[:, 1:]
    print(y)
    print(x)

    cv = cross_validation.KFold(len(y), n_folds=5, shuffle=True, random_state=42)
    accuracies = get_accuracy(cv, x, y)
    save_answer("q1.txt", accuracies.index[0])
    save_answer("q2.txt", accuracies.values[0])

    x = preprocessing.scale(x)
    accuracies = get_accuracy(cv, x, y)
    save_answer("q3.txt", accuracies.index[0])
    save_answer("q4.txt", accuracies.values[0])

main()
