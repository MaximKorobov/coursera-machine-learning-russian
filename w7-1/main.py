import datetime
import pandas
import os
import numpy
from sklearn.cross_validation import KFold, cross_val_score
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression

# Привет, Илья Пьявкин! ;)
#     (c) Макс Коробов

# 1. Считайте таблицу с признаками из файла features.csv с помощью кода, приведенного выше. Удалите признаки,
train = pandas.read_csv('./data/features.csv', index_col='match_id')
test = pandas.read_csv('./data/features_test.csv', index_col='match_id')

# 2. Проверьте выборку на наличие пропусков с помощью функции count(),
# которая для каждого столбца показывает число заполненных значений.
# Много ли пропусков в данных? Запишите названия признаков, имеющих пропуски,
# и попробуйте для любых двух из них дать обоснование, почему их значения могут быть пропущены.
stats = train.describe()
rows = len(train)

total_count = stats.T['count']
broken_count = total_count[total_count < rows]
normalized_stat = broken_count.sort_values().apply(lambda c: (rows - c) / rows)

print(normalized_stat)

#
# Градиентный бустинг "в лоб"
#
# 1. ... Удалите признаки, связанные с итогами матча (они помечены в описании данных как отсутствующие в тестовой
# выборке).

train.drop(['duration',
            'tower_status_radiant',
            'tower_status_dire',
            'barracks_status_radiant',
            'barracks_status_dire'
            ], axis=1, inplace=True)

X = train
y = train['radiant_win'].to_frame()
del train['radiant_win']


# Замените пропуски на нули с помощью функции fillna(). На самом деле этот способ является предпочтительным для
# логистической регрессии, поскольку он позволит пропущенному значению не вносить никакого вклада в предсказание.
# Для деревьев часто лучшим вариантом оказывается замена пропуска на очень большое или очень маленькое значение —
# в этом случае при построении разбиения вершины можно будет отправить объекты с пропусками в отдельную ветвь
# дерева. Также есть и другие подходы — например, замена пропуска на среднее значение признака.
# Мы не требуем этого в задании, но при желании попробуйте разные подходы к обработке пропусков и сравните
# их между собой.

# Замена пропусков данных
def clean(x):
    return x.fillna(0)


# Удаление категориальных признаков, то есть, тех, значения которых нельзя сравнивать между собой
def clean_category(x):
    x = clean(x)
    del x['lobby_type']
    for n in range(1, 6):
        del x['r{}_hero'.format(n)]
        del x['d{}_hero'.format(n)]

    return x


heroes = pandas.read_csv('./data/dictionaries/heroes.csv')
print('Heroes count:', len(heroes))


# Формирование мешка слов по героям
def hero_bag(x):
    x_pick = numpy.zeros((x.shape[0], len(heroes)))
    for i, match_id in enumerate(x.index):
        for p in range(5):
            x_pick[i, x.ix[match_id, 'r%d_hero' % (p + 1)] - 1] = 1
            x_pick[i, x.ix[match_id, 'd%d_hero' % (p + 1)] - 1] = -1

    return pandas.DataFrame(x_pick, index=x.index)


# ФАЙЛЫ
base_path = './data/prepared/'


# Сохранение подготовленных данных
def save_clean_data(cleaner, x_train, y_train, x_test, name='simple'):
    path = base_path + name
    if not os.path.exists(path):
        os.makedirs(path)

    y_train_path = path + '/y_train.csv'
    if not os.path.exists(y_train_path):  # Оптимизация: подготавливать файлы с данными только один раз
        y_train.to_csv(y_train_path)

    x_train_path = path + '/X_train.csv'
    if not os.path.exists(y_train_path):
        cleaner(x_train).to_csv(x_train_path)

    x_test_path = path + '/X_test.csv'
    if not os.path.exists(x_test_path):
        cleaner(x_test).to_csv(x_test_path)


# Загрузка подготовленных данных
def get_clean_data(cleaner_name='simple'):
    path = base_path + cleaner_name
    x_train = pandas.read_csv(path + '/X_train.csv', index_col='match_id')
    y_train = pandas.read_csv(path + '/y_train.csv', index_col='match_id')
    x_test = pandas.read_csv(path + '/X_test.csv', index_col='match_id')
    return x_train, y_train['radiant_win'], x_test


save_clean_data(clean, X, y, test)
save_clean_data(clean_category, X, y, test, name='clean_category')
save_clean_data(hero_bag, X, y, test, name='hero_bag')

# 3. Как долго проводилась кросс-валидация для градиентного бустинга с 30 деревьями?
# Инструкцию по измерению времени можно найти ниже по тексту. Какое качество при этом получилось?
# Напомним, что в данном задании мы используем метрику качества AUC-ROC.

# 4. Имеет ли смысл использовать больше 30 деревьев в градиентном бустинге?
# Что можно сделать, чтобы ускорить его обучение при увеличении количества деревьев?
x, y, x_kaggle = get_clean_data()
kf = KFold(y.size, n_folds=5, shuffle=True, random_state=42)
scores = []
nums = [10, 20, 30, 50, 100, 250]
for n in nums:
    print('#', str(n))
    model = GradientBoostingClassifier(n_estimators=n, random_state=42)
    start_time = datetime.datetime.now()
    model_scores = cross_val_score(model, X, y, cv=kf, scoring='roc_auc', n_jobs=-1)
    print('Time elapsed:', datetime.datetime.now() - start_time)
    print(model_scores)
    scores.append(numpy.mean(model_scores))

#
# Логистическая регрессия
#

x, y, x_kaggle = get_clean_data()
scaler = StandardScaler()
x = scaler.fit_transform(X)

kf = KFold(y.size, n_folds=5, shuffle=True, random_state=42)


def test_model_C(X, y, C):
    print('C=', str(C))
    model = LogisticRegression(C=C, random_state=42, n_jobs=-1)
    return cross_val_score(model, X, y, cv=kf, scoring='roc_auc', n_jobs=-1)


def test_model(x, y):
    local_scores = []
    c_pow_range = range(-5, 6)
    c_range = [10.0 ** i for i in c_pow_range]
    for C in c_range:
        start_time = datetime.datetime.now()
        model_scores = test_model_C(x, y, C)
        print(model_scores)
        print('Time elapsed:', datetime.datetime.now() - start_time, local_scores.append(numpy.mean(model_scores)))

    max_score = max(local_scores)
    max_score_index = local_scores.index(max_score)
    return c_range[max_score_index], max_score

C, score = test_model(x, y)
print("C: {0}; score: {1}".format(C, score))


# Удаление категориальных признаков
x, y, x_kaggle = get_clean_data('clean_category')
scaler = StandardScaler()
X = scaler.fit_transform(X)

kf = KFold(y.size, n_folds=5, shuffle=True, random_state=42)

C, score = test_model(x, y)
print("C: {0}; score: {1}".format(C, score))

# "Мешок слов" для кодирования информации о героях.
x, y, x_kaggle = get_clean_data('clean_category')
x_hero, _y, x_kaggle_hero = get_clean_data('hero_bag')

scaler = StandardScaler()
x = pandas.DataFrame(scaler.fit_transform(x), index = x.index)
x_kaggle = pandas.DataFrame(scaler.transform(x_kaggle), index = x_kaggle.index)

x = pandas.concat([x, x_hero], axis=1)
x_kaggle = pandas.concat([x_kaggle, x_kaggle_hero], axis=1)

kf = KFold(y.size, n_folds=5, shuffle=True, random_state=42)

C, score = test_model(x, y)
print("C: {0}; score: {1}".format(C, score))
