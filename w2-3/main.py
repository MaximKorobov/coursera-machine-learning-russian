import pandas
from sklearn.linear_model import Perceptron
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler


def main():
    train = pandas.read_csv("perceptron-train.csv", header=None)
    train_y = train[0]
    train_x = train.loc[:, 1:]

    test = pandas.read_csv("perceptron-test.csv", header=None)
    test_y = test[0]
    test_x = test.loc[:, 1:]

    model = Perceptron(random_state=241)
    model.fit(train_x, train_y)
    train_accuracy = accuracy_score(test_y, model.predict(test_x))

    scaler = StandardScaler()
    train_x_scaled = scaler.fit_transform(train_x)
    test_x_scaled = scaler.transform(test_x)

    model = Perceptron(random_state=241)
    model.fit(train_x_scaled, train_y)
    test_accuracy = accuracy_score(test_y, model.predict(test_x_scaled))

    print("Accuracy diff: {0}".format(test_accuracy - train_accuracy))

main()
