import pandas
from sklearn.tree import DecisionTreeClassifier


def main():
    all_data = pandas.read_csv("titanic.csv", index_col="PassengerId")

    # Transform sex
    prepared_data = all_data[["Pclass", "Fare", "Age", "Sex", "Survived"]].replace(['male', 'female'], [1, 0])
    # Remove empty ages
    prepared_data = prepared_data[prepared_data["Age"].notnull()]
    print(prepared_data)

    X = prepared_data[["Pclass", "Fare", "Age", "Sex"]]
    Y = prepared_data["Survived"]
    clf = DecisionTreeClassifier(random_state=241)
    clf.fit(X, Y)
    importance = clf.feature_importances_
    print(importance)
main()
