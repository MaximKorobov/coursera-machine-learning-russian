import pandas
from sklearn.svm import SVC


def save_answer(filename, text):
    print("Save to {0}: {1}".format(filename, text))
    text_file = open(filename, "w")
    text_file.write(str(text))
    text_file.close()


def main():
    data = pandas.read_csv("svm-data.csv", header=None)
    y = data[0]
    x = data.loc[:, 1:]

    model = SVC(C=100000, random_state=241, kernel='linear')
    model.fit(x, y)

    result = ""
    for support in model.support_:
        if result != "":
            result += " "
        result += str(support + 1)

    save_answer("q1.txt", result)

main()
