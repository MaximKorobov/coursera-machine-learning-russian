import numpy as np
from pandas import DataFrame
from sklearn import datasets, grid_search
from sklearn.cross_validation import KFold
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.svm import SVC


def main():
    data = datasets.fetch_20newsgroups(subset='all', categories=['alt.atheism', 'sci.space'])
    x = data.data
    y = data.target

    # train
    vectorizer = TfidfVectorizer()
    vectorizer.fit_transform(x)

    # feature_mapping = vectorizer.get_feature_names()
    # for feature in feature_mapping:
    #     print("{0}".format(feature))

    # test
    test_x = vectorizer.transform(x)
    grid = {'C': np.power(10.0, np.arange(-5, 6))}
    cv = KFold(y.size, n_folds=5, shuffle=True, random_state=241)
    model = SVC(kernel='linear', random_state=241)
    gs = grid_search.GridSearchCV(model, grid, scoring='accuracy', cv=cv)
    gs.fit(test_x, y)

    # Начиная отсюда списал...
    score = 0
    C = 0
    for suggest in gs.grid_scores_:
        if suggest.mean_validation_score > score:
            score = suggest.mean_validation_score
    C = suggest.parameters['C']

    model = SVC(kernel='linear', random_state=241, C=C)
    model.fit(test_x, y)

    words = vectorizer.get_feature_names()
    coef = DataFrame(model.coef_.data, model.coef_.indices)
    top_words = coef[0].map(lambda w: abs(w)).sort_values(ascending=False).head(10).index.map(lambda i: words[i])
    top_words.sort()
    print(','.join(top_words))
main()
